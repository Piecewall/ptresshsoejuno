/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shellsortpractres;

/**
 *
 * @author Ricardo Cach
 */
import java.util.Scanner;
public class ShellSortPracTres {

    
    
    public static void main(String[] args) {
        Scanner valor = new Scanner(System.in);
        System.out.println("Ingrese número de elementos");
        int p = valor.nextInt(); //se define por el usuario el tamaño del arreglo
        int[]a = new int[p]; //ese tamaño definido por el usuario se establece en el arreglo
        System.out.println("Ingrese los valores del arreglo");
        for(int i = 0; i<p; i++){ //se hace un for para ir recibiendo los valores
        a[i] = valor.nextInt(); //el arreglo tendrá elementos de un tipo por el usuario
        }
        System.out.println("Resultados:");
        shellsort(a); //llamado del metodo al main
        for(int i = 0;i<p; i++){ //for para ir mostrando los números
        System.out.println(+a[i]);
        }
        
    }
    public static void shellsort(int[] matrix){
    for(int increment = matrix.length/2; increment > 0; increment = //se pone el incremento
            (increment == 2) ? 1: (int) Math.round(increment/2.2)){ //se redondea el resultado
            for(int i = increment; i<matrix.length; i++){ //el tamaño se define y este debe ser mayor a i
            for(int j=i; j>= increment && matrix[j-increment] //se reemplaza la j por i
                    >matrix[j]; j -= increment){ //se resta la j al incremento
            int temp = matrix[j]; //una variable temporal reemplazará el tamaño del arreglo 
            matrix[j] = matrix[j - increment]; //se define cómo será el tamaño
            matrix[j - increment] = temp;// al final la variable temp queda como el arreglo con el nuevo tamaño
            }

            }
      
    }
    
    }
}
